#include "pilha.h"
#include <stdlib.h>

elem * create_plate(int val)
{
    elem * newPlate;

    if((newPlate = (elem *)malloc(sizeof(elem))) != NULL)
    {
        newPlate->val = val;
        newPlate->next = NULL;
        return newPlate;
    } else
        return NULL;
}

/*          */

struct pilha * create()
{
    struct pilha * newStack;

    if((newStack = (struct pilha *)malloc(sizeof(struct pilha))) != NULL)
    {
        newStack->tam = 0;
        if((newStack->cabeca = (elem *)malloc(sizeof(elem))) != NULL)
            {
                newStack->cabeca->next = NULL;
                return newStack;
            } else
                return NULL;
    } else
        return NULL;
}

int makenull(struct pilha * p)
{
    p->cabeca->next = NULL;
    return 1;
}

int top(struct pilha * p)
{
    if(p->cabeca->next != NULL)
        return p->cabeca->next->val;
    else
        return 0;
}

int pop(struct pilha * p)
{
    elem * del;
    int val;

    val = p->cabeca->next->val;

    if(p->tam == 1)
        del = p->cabeca;
    else
    {
        del = p->cabeca;
        p->cabeca = p->cabeca->next;
    }

    free(del);
    p->tam--;

    if(val == p->cabeca->val || p->tam == 0)
        return 1;
    else
        return 0;

}

int push(struct pilha * p, int val)
{
    elem * newPlate, * temp;
    newPlate = create_plate(val);


    temp = p->cabeca->next;
    p->cabeca->next = newPlate;
    newPlate->next = temp;
    p->tam++;

    if(p->cabeca->next->val == val)
        return 1;
    else
        return 0;
}

int vazia(struct pilha * p)
{
    if(p->tam == 0)
        return 1;
    else
        return 0;
}

void destroy(struct pilha * p)
{
    makenull(p);
    free(p);
}
