#include "fila.h"
#include <stdlib.h>

struct fila * create()
{
    struct fila * q;

    if((q = (struct fila *)malloc(sizeof(struct fila))) != NULL)
    {
        q->tam = 0;
        if((q->header = (elem *)malloc(sizeof(elem))) != NULL)
        {
            q->header->next = q->header;
            q->header->prev = q->header;
            q->header->val = -1;
            return q;
        }
        else
            return NULL;
    } else
        return NULL;
}

int makenull(struct fila * f)
{
    if(f->tam == 0)
        return 0;
    else
    {
        while(dequeue(f));
        return 1;
    }
}

int dequeue(struct fila * f)
{
    elem * deq;
    int val;

    if(f->tam == 0)
        return 0;
    else if(f->tam == 1)
    {
        deq = f->header->next;
        val = deq->val;
        f->header->next = f->header;
        f->header->prev = f->header;
    } else {
        deq = f->header->next;
        val = deq->val;
        f->header->next = deq->next;
        deq->prev = f->header;
    }
    
    f->tam--;
    free(deq);
    return val;
}

int enqueue(struct fila * f, int val)
{
    elem * node, * temp;

    if((node = (elem *)malloc(sizeof(elem))) != NULL)
    {
        node->val = val;
        
        if(f->tam == 0)
        {
            f->header->next = node;
            f->header->prev = node;

            node->next = f->header;
            node->prev = f->header;
        } else {
            temp = f->header->prev;

            temp->next = node;
            node->prev = temp;

            f->header->prev = node;
            node->next = f->header;
        }
        f->tam++;
        return 1;
    } else
        return 0;
}

int vazia(struct fila * f)
{
    if(f->tam == 0)
        return 1;
    else
        return 0;
}

void destroy(struct fila * f)
{
    free(f);
}