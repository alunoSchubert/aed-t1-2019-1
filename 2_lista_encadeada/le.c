#include "le.h"
#include <stdlib.h>

/* Aqui devem ser implementadas as funções definidas em le.h */

struct llist * create_l()
{
    struct llist * lista;

    if((lista = (struct llist *)malloc(sizeof(struct llist))) != NULL)
    {
        lista->cabeca = NULL;
        lista->tam = 0;
        return lista;
    } else
        return NULL;
}

elem * create_node(int val)
{
    elem * novoNodo;

    if((novoNodo = (elem *)malloc(sizeof(elem))) != NULL)
    {
        novoNodo->val = val;
        novoNodo->next = NULL;
        return novoNodo;
    } else
        return NULL;
}

int insert_l(struct llist *desc, elem * prev, elem * item)
{
    elem * tempNodo;
    
    if(prev == NULL)
    {
        tempNodo = desc->cabeca;
        desc->cabeca = item;
        item->next = tempNodo;
        desc->tam++;
        return 1;
    } else {
        tempNodo = desc->cabeca;
        while(tempNodo->next != NULL)
            tempNodo->next = tempNodo->next->next;
        return 1;
    }

    // if(prev == NULL)
    //     if((desc->cabeca) != NULL)
    //     {
    //         tempNodo = desc->cabeca;
    //         desc->cabeca = item;
    //         item->next = tempNodo;
    //         desc->tam++;
    //         return 1;
    //     } else {
    //         desc->cabeca = item;
    //         desc->tam = 1;
    //         return 1;
    //     }
    // else
    // {
    //     for(i=0, tempNodo=desc->cabeca; tempNodo->next!=NULL; i++, tempNodo->next++);
    //     item = tempNodo->next;
    //     desc->tam++;
    //     return 1;
    // }
}

int delete_l(struct llist *desc, elem * prev)
{
    elem * tempNodo;

    if(((prev==NULL) && (prev!=desc->cabeca)) || (prev->next==NULL))
        return 0;
    else
    {
        tempNodo = prev->next;
        prev->next = prev->next->next;
        free(tempNodo);
        desc->tam--;
        return 1;
    }
}

elem * get_l(struct llist *desc, int pos)
{
    elem * tempNodo;
    int i;

    if((pos<0) || (pos>desc->tam))
        return 0;
    else
    {
        tempNodo = desc->cabeca;
        i = pos-1;
        while((i>0) && (tempNodo!=NULL))
        {
            i--;
            tempNodo = tempNodo->next;
        }
        if(tempNodo == NULL)
            return 0;
        else
            return tempNodo;
    }
}

int set_l(struct llist *desc, int pos, int val)
{
    elem * tempNodo;

    tempNodo = get_l(desc, pos);
    if(tempNodo)
    {
        tempNodo->val = val;
        return 1;
    }
    else
        return 0;
}

elem * locate_l(struct llist *desc, elem * prev, int val)
{
    elem * tempNodo;

    if(prev == NULL)
    {
        tempNodo = desc->cabeca;
        while(tempNodo != NULL)
        {
            if(tempNodo->val == val)
                return tempNodo;
            else
                tempNodo = tempNodo->next;
        }
    } else {
        tempNodo = prev;
        while(tempNodo != NULL)
        {
            if(tempNodo->val == val)
                return tempNodo;
            else
                tempNodo = tempNodo->next;
        }
    }

    return NULL;
}

int length_l(struct llist *desc)
{
    return desc->tam;
}

void destroy_l(struct llist *desc)
{
    free(desc);
}