#include "ls.h"
#include <stdlib.h>

/* Aqui devem ser implementadas as funções definidas em ls.h */

struct list * create(int max)
{
    struct list * allData;

    if(((allData = (struct list *)malloc(sizeof(struct list))) != NULL) && ((allData->arm = (elem *)malloc(sizeof(elem) * max)) != NULL) && (max!=0))
    {
        allData->ultimo = 0;
        allData->capacidade = max;        
        return allData;
    }
    else
        return NULL;
}

int insert(struct list *desc, int pos, elem item)
{
    if((pos>desc->capacidade))
        return 0;
    else
    {
        desc->arm[pos] = item;
        desc->ultimo += 1;
        return 1;
    }

}

int removel(struct list *desc, int pos)
{
    int i;

    if(pos > desc->ultimo)
        return 0;
    else
    {
        desc->ultimo -= 1;
        
        for(i=pos; i<=desc->ultimo; i++)
            desc->arm[i] = desc->arm[i+1];
        return 1;
    }
}

elem get(struct list *desc, int pos)
{
    if((pos>desc->ultimo) || (pos<1))
        return 0;
    else
        return desc->arm[pos];
}

int set(struct list *desc, int pos, elem item)
{
    if((pos>desc->ultimo) || (pos<1))
        return 0;
    else
    {
        desc->arm[pos] = item;
        return 1;
    }
}

int locate(struct list *desc, int pos, elem item)
{
    int i;

    for(i=pos; i<=desc->ultimo; i++)
        if(desc->arm[i] == item)
            return i;

    return 0;
}

int length(struct list *desc)
{
    return desc->ultimo;
}

int max(struct list *desc)
{
    return desc->capacidade;
}

int full(struct list *desc)
{
    if(length(desc) == max(desc))
        return 1;
    else return 0;
}

void destroy(struct list *desc)
{
    free(desc->arm);
    free(desc);
}